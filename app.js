const apm = require('elastic-apm-node');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const postsRoute = require('./routes/posts');
require('dotenv/config');

apm.start({
    secretToken: process.env.AMP_SECRET_TOKEN,
    serverUrl: process.env.AMP_SERVER_URL,
    environment: 'production'
});

const app = express();
const port = 3000;

app.use(bodyParser.json());

app.use('/posts', postsRoute);

mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log('connected')
);

app.listen(port);
